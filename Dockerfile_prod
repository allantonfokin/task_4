FROM golang:1.17-alpine AS builder

RUN apk update && apk add --no-cache git

#sert env vabiables
ENV USER=appuser
ENV UID=10001

#create user
RUN adduser \
--disabled-password \
--gecos "" \
--home "/nonexistent" \
--shell "/sbin/nologin" \
--no-create-home \
--uid "${UID}" "${USER}"

#set work dir & copy necessary files from host
WORKDIR /go/src/app
COPY go.mod *.go ./

#load the dependencies
RUN go mod download
RUN go mod verify

#build the app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/main

#use scratch image
FROM scratch AS prod

#copy files from builder
COPY --from=builder /go/bin/main /go/bin/main
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY static ./static

#set the user with low privileges
USER appuser:appuser

#listen 9001 port
EXPOSE 9001

#run the app
ENTRYPOINT ["/go/bin/main"]

